package com.example.soundpool;

import android.app.Activity;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {
	private SoundPool soundPool;
	private int soundID, soundID2;
	boolean plays = false, loaded = false;
	float actVolume, maxVolume;
	int counter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		Button play1 = (Button) findViewById(R.id.play1);
		Button play2 = (Button) findViewById(R.id.play2);
		Button stopAll = (Button) findViewById(R.id.stopAll);

		// Carrega els sons
		soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
		soundPool.setOnLoadCompleteListener(new OnLoadCompleteListener() {
			public void onLoadComplete(SoundPool soundPool, int sampleId,
					int status) {
				loaded = true;
			}
		});
		soundID = soundPool.load(this, R.raw.r2d2, 1);
		soundID2 = soundPool.load(this, R.raw.laser_machine, 1);
	}

	public void playSound(View v) {
		// El so es repeteix indefinidament si indiquem -1 al paràmetre de loop
		if (loaded && counter <= 3) {
			soundPool.play(soundID, 5, 5, 1, -1, 1f);
			counter = counter++;

			plays = true;
		}
	}
	
	public void playSound2(View v) {
		// Si no hi ha més de 3 sons sonant alhora, comença un de nou
		if (loaded && counter <= 3) {
			soundPool.play(soundID2, 5, 5, 1, -1, 1f);
			counter = counter++;

			plays = true;
		}
	}

	public void stopSound(View v) {
		if (plays) {
			soundPool.stop(soundID);
			soundPool.stop(soundID2);
			soundID = soundPool.load(this, R.raw.r2d2, counter);
			soundID2 = soundPool.load(this, R.raw.laser_machine, counter);

			plays = false;
		}
	}
}
